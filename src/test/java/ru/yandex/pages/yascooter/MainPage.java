package ru.yandex.pages.yascooter;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import ru.yandex.pages.components.OrderBlock;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selectors.*;

public class MainPage extends BasePage<MainPage>{

    private OrderBlock orderBlock;

    public static MainPage enterToMainPage(){
        open("https://qa-scooter.praktikum-services.ru/");
        return page(MainPage.class);
    }

    public OrderBlock goToOrdering(){
        $(".Button_Button__ra12g").click();
        return page(OrderBlock.class);
    }

    public MainPage scrollToQuestions(){
        $(byClassName("accordion")).scrollTo();
        return this;
    }

    public MainPage goToOrderStatus(){
        $(".Header_Link__1TAG7").click();
        $x("//input[@placeholder='Введите номер заказа']").shouldBe(Condition.visible);
        return this;
    }

    public MainPage inputOrderNumber(int num){
        $x("//input[@placeholder='Введите номер заказа']").sendKeys(String.valueOf(num));
        $x("//button[@class='Button_Button__ra12g Header_Button__28dPO']").click();
        return this;
    }

    public Boolean orderIsExist(){
        return $x("//img[@alt='Not found']").shouldBe(Condition.visible).isDisplayed();
    }

    public MainPage viewAllImportantQuestions(){
        ElementsCollection elements = $$(byClassName("accordion__item"));
        for(SelenideElement element : elements){
            element.lastChild().shouldHave(Condition.cssClass("accordion__panel"));
            element.click();
        }
        return this;
    }


}
