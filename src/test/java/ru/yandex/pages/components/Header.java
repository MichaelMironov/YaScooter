package ru.yandex.pages.components;

import com.codeborne.selenide.SelenideElement;
import ru.yandex.pages.yascooter.BasePage;

import static com.codeborne.selenide.Selenide.$;

public class Header extends BasePage<Header> {

    private SelenideElement scooterLogo = $(".Header_LogoScooter__3lsAR");
    private SelenideElement yandexLogo = $(".Header_LogoYandex__3TSOI");

    public SelenideElement getScooterLogo() {
        return scooterLogo;
    }

    public SelenideElement getYandexLogo() {
        return yandexLogo;
    }
}
