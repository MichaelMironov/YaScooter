package ru.yandex.pages.components;

import com.codeborne.selenide.selector.ByText;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selectors.*;
import ru.yandex.pages.yascooter.BasePage;
import ru.yandex.pages.yascooter.MainPage;
import com.codeborne.selenide.*;

public class OrderBlock extends BasePage<OrderBlock> {

    private SelenideElement firstname = $x("//input[@placeholder='* Имя']");
    private SelenideElement lastname = $x("//input[@placeholder='* Фамилия']");
    private SelenideElement address = $x("//input[@placeholder='* Адрес: куда привезти заказ']");
    private SelenideElement metro = $x("//input[@placeholder='* Станция метро']");
    private SelenideElement phoneNumber = $x("//input[@placeholder='* Телефон: на него позвонит курьер']");

    public Boolean validationIsDisplayed(SelenideElement element){
        String fullPath = element.getSearchCriteria();
        String[] xpath = fullPath.split(" ",2);
        return  $x(xpath[1] + "/following-sibling::div").isDisplayed();
    }

    public SelenideElement getFirstname() {
        return firstname;
    }

    public SelenideElement getLastname() {
        return lastname;
    }

    public SelenideElement getAddress() {
        return address;
    }

    public SelenideElement getMetro() {
        return metro;
    }

    public SelenideElement getPhoneNumber() {
        return phoneNumber;
    }

    public OrderBlock inputName(String name){
        firstname.setValue(name);
        return page(OrderBlock.class);
    }

    public OrderBlock inputLastname(String surname){
        lastname.setValue(surname);
        return page(OrderBlock.class);
    }

    public OrderBlock inputAddress(String addressDelivery){
        address.setValue(addressDelivery);
        return page(OrderBlock.class);
    }

    public OrderBlock chooseMetro(String closestMetro) {
        metro.setValue(closestMetro);
        $(".select-search__select").click();
        return page(OrderBlock.class);
    }

    public OrderBlock inputPhoneNumber(String number){
        phoneNumber.setValue(number);
        return page(OrderBlock.class);
    }

    public OrderBlock nextButton(){
        $x("//button[@class='Button_Button__ra12g Button_Middle__1CSJM']").click();
        return page(OrderBlock.class);
    }

    public OrderBlock deliveryDate(String date){
        $x("//input[@placeholder='* Когда привезти самокат']").setValue(date).pressEnter();
        return page(OrderBlock.class);
    }

    public OrderBlock rentalPeriod(String term){

        $(".Dropdown-placeholder").click();
        $(new ByText(term)).click();
        return page(OrderBlock.class);
    }

    public OrderBlock selectColor(String color){
        $(".Order_Checkboxes__3lWSI").find(new ByText(color)).click();
        return page(OrderBlock.this);
    }

    public MainPage confirmTheOrder(){
        $(byClassName("Order_Modal__YZ-d3")).shouldBe(Condition.visible).lastChild().lastChild().click();
        $(byClassName("Order_Modal__YZ-d3")).shouldBe(Condition.not(Condition.visible));

        return page(MainPage.class);
    }
}
