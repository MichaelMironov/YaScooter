package ru.yandex.test;

import com.codeborne.selenide.Configuration;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import ru.yandex.pages.components.Header;
import ru.yandex.pages.components.OrderBlock;
import java.util.stream.Stream;
import static com.codeborne.selenide.Selenide.closeWebDriver;
import static com.codeborne.selenide.Selenide.switchTo;
import static com.codeborne.selenide.WebDriverRunner.url;
import static ru.yandex.pages.yascooter.MainPage.enterToMainPage;

public class BaseTest {
    @BeforeAll
    static void BeforeAll(){
        Configuration.browser = "chrome";
        Configuration.timeout = 10000;
        WebDriverManager.chromedriver().setup();
    }

    static Stream<Arguments> dateForOrdering(){
        return Stream.of(
                Arguments.arguments("Михаил", "Миронов", "Ул.Ленина", "Лубянка","88005553535", "07.08.22", "сутки", "чёрный жемчуг"),
                Arguments.arguments("Михаил","Анточ", "Ул.Сталина", "Выхино", "88005553535", "23.12.24", "пятеро суток", "серая безысходность"),
                Arguments.arguments("Чухлебов", "Лев", "Ул.Московская", "Котельники", "89233458988", "01.10.23", "семеро суток", "чёрный жемчуг"),
                Arguments.arguments("Чернявский", "Павел", "Ул.Железнодорожная", "Выхино", "89293458988", "31.01.23", "четверо суток", "чёрный жемчуг"),
                Arguments.arguments("Точеный", "Григорий", "Ул.Длинная", "Динамо", "89157589788", "01.09.22", "двое суток", "чёрный жемчуг")
        );
    }

    @Disabled
    @ParameterizedTest
    @MethodSource("dateForOrdering")
    void OrderTest(String firstname, String lastname, String address, String metro, String number, String date, String period, String color) {
        enterToMainPage()
                .goToOrdering()
                .inputName(firstname)
                .inputLastname(lastname)
                .inputAddress(address)
                .chooseMetro(metro)
                .inputPhoneNumber(number)
                .nextButton()
                .deliveryDate(date)
                .rentalPeriod(period)
                .selectColor(color)
                .nextButton()
                .confirmTheOrder();
    }

    @Disabled
    @Test
    void questionsTest() {
        enterToMainPage()
                .scrollToQuestions()
                .viewAllImportantQuestions();
    }

    @Disabled
    @Test
    void validatorsTest(){
        enterToMainPage()
                .goToOrdering()
                .nextButton();

        OrderBlock orderBlock = new OrderBlock();
        Assertions.assertTrue(orderBlock.validationIsDisplayed(orderBlock.getFirstname()));
        Assertions.assertTrue(orderBlock.validationIsDisplayed(orderBlock.getLastname()));
        Assertions.assertTrue(orderBlock.validationIsDisplayed(orderBlock.getAddress()));
    }

    @Disabled
    @Test
    void yaScooterLogoTest(){
        enterToMainPage();
        Header header = new Header();
        header.getScooterLogo().click();
        Assertions.assertEquals("https://qa-scooter.praktikum-services.ru/",url());
    }

    @Disabled
    @Test
    void yandexLogoTest(){
        enterToMainPage();
        Header header = new Header();
        header.getYandexLogo().click();
        switchTo().window(1);
        Assertions.assertEquals("https://yandex.ru/", url());
    }

    @Test
    void checkIncorrectOrder(){

        Assertions.assertTrue(
        enterToMainPage()
                .goToOrderStatus()
                .inputOrderNumber(777)
                .orderIsExist());
    }
    @AfterAll
    static void After(){
        closeWebDriver();
    }
}
